ifconfig eth0 220.189.232.50/24
ifconfig eth1 220.189.234.254/30
route add default gw 220.189.234.253

# Todo el trafico saliente, lo hace por la 220.189.234.253
ip route add 0.0.0.0/0 via 220.189.234.253 metric 1