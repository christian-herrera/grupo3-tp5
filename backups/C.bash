ifconfig eth0 220.189.234.250/30
ifconfig eth1 220.189.234.245/30
ifconfig eth2 220.189.234.129/26
ifconfig eth3 220.189.234.97/27

#subred A
ip route add 220.189.234.64/27 via 220.189.234.249 dev eth0 metric 1
ip route add 220.189.234.64/27 via 220.189.234.246 dev eth1 metric 2

#subred B
ip route add 220.189.234.192/27 via 220.189.234.246 dev eth1 metric 1
ip route add 220.189.234.192/27 via 220.189.234.249 dev eth0 metric 2

#Administracion
ip route add 220.189.234.0/26 via 220.189.234.249 dev eth0 metric 1
ip route add 220.189.234.0/26 via 220.189.234.246 dev eth1 metric 2

#Internet
ip route add 220.189.232.0/24 via 220.189.234.249 dev eth0 metric 1
ip route add 220.189.232.0/24 via 220.189.234.246 dev eth1 metric 2