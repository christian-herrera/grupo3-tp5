ifconfig eth1 220.189.234.1/26
ifconfig eth3 220.189.234.249/30
ifconfig eth4 220.189.234.242/30
ifconfig eth2 220.189.234.65/27
ifconfig eth0 220.189.234.253/30

#subred C
ip route add 220.189.234.96/27 via 220.189.234.250 dev eth3 metric 1
ip route add 220.189.234.96/27 via 220.189.234.241 dev eth4 metric 2

#subred B
ip route add 220.189.234.192/27 via 220.189.234.241 dev eth4 metric 1
ip route add 220.189.234.192/27 via 220.189.234.250 dev eth3 metric 2

#Mantenimiento
ip route add 220.189.234.128/26 via 220.189.234.250 dev eth3 metric 1
ip route add 220.189.234.128/26 via 220.189.234.241 dev eth4 metric 2

#Internet
ip route add 220.189.232.0/24 via 220.189.234.254 dev eth0

