ifconfig eth0 220.189.234.246/30
ifconfig eth1 220.189.234.241/30
ifconfig eth2 220.189.234.193/27

#subred A
ip route add 220.189.234.64/27 via 220.189.234.242 dev eth1 metric 1
ip route add 220.189.234.64/27 via 220.189.234.245 dev eth0 metric 2

#subred C
ip route add 220.189.234.96/27 via 220.189.234.245 dev eth0 metric 1
ip route add 220.189.234.96/27 via 220.189.234.242 dev eth1 metric 2

#Administracion
ip route add 220.189.234.0/26 via 220.189.234.242 dev eth1 metric 1
ip route add 220.189.234.0/26 via 220.189.234.245 dev eth0 metric 2

#Mantenimiento
ip route add 220.189.234.128/26 via 220.189.234.245 dev eth0 metric 1
ip route add 220.189.234.128/26 via 220.189.234.242 dev eth1 metric 2

#Internet
ip route add 220.189.232.0/24 via 220.189.234.242 dev eth1 metric 1
ip route add 220.189.232.0/24 via 220.189.234.245 dev eth0 metric 2