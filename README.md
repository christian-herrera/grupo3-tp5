# SOyR - Trabajo Practico Nro. 5

![Static Badge](https://img.shields.io/badge/status-stable-green)
![Static Badge](https://img.shields.io/badge/version-1.2-blue)

## Enunciado

En la planta de una empresa hay tres edificios/sectores: Administracion (sector A), Produccion (sector B) y Mantenimiento (sector C). Se contrata un servicio para proveer internet, y se quiere disear la interconexion de las dependencias. Se debera cumplir con:

 - A cada dependencia, se le asignaran subredes cableadas con disponibilidad para 30 direcciones IP en cada una.
 - Para el sector administrativo y de mantenimiento, se le asignaran subredes con disponibilidad para 62 direcciones IP cada una.
 - El proveedor del servicio de internet instala la conexion en el primer edificio (edificio A). Deja un router que desde el lado externo tiene la IP 220.189.232.50, con el GateWay: 220.189.232.1.
 - Del lado interno de la empresa, provee la subred clase C: 220.189.234./24. La direccion asignada al router es 220.189.234.254.

Entonces, se podra hacer la siguiente topologia:

![](/docs/images/topologia.svg){width="100%"}

# Resultados

 - [x] Creacion de 2 subredes de 64 direcciones IP (62 utilizables).
 - [x] Creacion de 3 subredes de 32 direcciones IP (30 utilizables).
 - [x] Creacion de 4 subredes de 4 direcciones IP (2 utilizables).
 - [x] Ping funcionando con todas las IPs desde cualquier host.
 - [x] Metricas implementadas para asegurar la conexion ante caidas en las conexiones.
 - [x] Acceso a internet en todos los hosts.

## Subredes creadas

|   **Subred**   | **Mask** |  **IP Inicial** |   **IP Final**  | **Cant. de IPs** |
|:--------------:|:--------:|:---------------:|:---------------:|:----------------:|
|        -       |    30    | 220.189.234.252 | 220.189.234.255 |         4        |
|        -       |    30    | 220.189.234.248 | 220.189.234.251 |         4        |
|        -       |    30    | 220.189.234.244 | 220.189.234.243 |         4        |
|        -       |    30    | 220.189.234.240 | 220.189.234.243 |         4        |
| Administracion |    26    |  220.189.234.0  |  220.189.234.63 |        64        |
|    SubRed A    |    27    |  220.189.234.64 |  220.189.234.95 |        32        |
|  Mantenimiento |    26    | 220.189.234.128 | 220.189.234.191 |        64        |
|    SubRed C    |    27    |  220.189.234.96 | 220.189.234.127 |        32        |
|   Produccion   |    27    | 220.189.234.192 | 220.189.234.223 |        32        |


Como resultado de las direcciones ip utilizadas, se tiene que quedan 16 direcciones ip sin asignar, es decir. Las ip en el rango (incluyendo los extremos) seran:

    220.189.234.224 ~ 220.189.234.239

Lo que da el resultado de 16 direcciones ip.

